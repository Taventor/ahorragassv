<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGasolinerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasolineras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_comercial');
            $table->string('razon_social');
            $table->string('departamento');
            $table->string('municipio');
            $table->string('direccion');
            $table->string('latitud');
            $table->string('longitud');
            $table->text('mapa');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasolineras');
    }
}
