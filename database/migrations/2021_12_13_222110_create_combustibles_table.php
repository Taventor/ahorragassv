<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCombustiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combustibles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_gasolinera');
            $table->string('gasolina_regular');
            $table->double('precio_gr');
            $table->string('gasolina_especial');
            $table->double('precio_gs');
            $table->string('gasolina_diesel');
            $table->double('precio_diesel');
            $table->timestamps();

            $table->foreign('id_gasolinera')->references('id')->on('gasolineras')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('combustibles');
    }
}
