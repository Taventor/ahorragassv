@extends('adminlte::page')

@section('title', 'Detalle de rol')

@section('content_header')
<h1>Detalles de rol</h1>
@stop

@section('content')

<div class="container"></div>
<div class="card">
    <div class="card-header">
        <h3>{{ __('Name') }} {{ $rol -> name }}</h3>
        <h4>{{ __('Slug') }} {{ $rol -> slug }}</h4>
    </div>
    <div class="card-body">

    </div>
    <div class="card-footer">
        <a href="{{ url() -> previous() }}" class="btn btn-dark"><i class="far fa-arrow-alt-circle-left"></i>
            Regresar</a>
    </div>
</div>
</div>

@endsection