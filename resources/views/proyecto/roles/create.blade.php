@extends('adminlte::page')

@section('title', 'Creación de rol')

@section('content_header')

<h1>Crear nuevo rol</h1>

@stop

@section('css')
<link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
@stop

@section('js')
<script src="/js/bootstrap-tagsinput.js"></script>
<script>
    $(document).ready(function() {
            $('#name').keyup(function(e) {
                var str = $('#name').val();
                str = str.replace(/\W+(?!$)/g, '-').toLowerCase();
                $('#slug').val(str);
                $('#slug').attr('placeholder', str);
            });
        });
</script>
@stop

@section('plugins.bsCustomFileInput', true)

@section('content')

<div class="container-fluid bg-gradient-light">
    <div class="row justify-content-center">
        <div class="card bg-gradient-blue col-lg-5 m-3">
            <div class="card-header text-center">
                <h3>Crear nuevo rol de sistema</h3>
            </div>
            <div class="card-body bg-light">
                <form action="/roles" method="post" enctype="multipart/form-data">
                    @csrf()

                    <x-adminlte-input name="name" label="Name" placeholder="Digite el nombre del rol"
                        label-class="text-lightblue" value="{{ old('name') }}" required>
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                    </x-adminlte-input>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <x-adminlte-input name="slug" label="Slug" placeholder="Digite el nombre del Slug" label-class="text-lightblue"
                        value="{{ old('slug') }}" required>
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                    </x-adminlte-input>
                    @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <x-adminlte-input name="permissions" label="Permisos" placeholder="Agrege permisos" label-class="text-lightblue"
                        value="" data-role="tagsinput">
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('permissions')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>

                    <x-adminlte-button label="Crear" theme="dark" icon="fas fa-user-check" class="m-2 float-center"
                        type="submmit" />
                </form>
            </div>
            <card-footer>
                <a href="{{ url() -> previous() }}" class="btn btn-dark m-2 float-right"><i
                        class="far fa-arrow-alt-circle-left"></i> Regresar</a>
            </card-footer>
        </div>
    </div>
</div>

@endsection