@extends('adminlte::page')

@section('title', 'Maestro de roles')

@section('content_header')

<h1>Administración de roles</h1>

@stop

@section('plugins.Datatables', true)

@section('plugins.DatatablesPlugins', true)

@section('plugins.select2', true)

@section('js')

<script>
    $(function () {
    $('[data-toggle="tooltip"]').tooltip()
    });
</script>

<script>
    $('#deleteRol').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var rol_id = button.data('rolid')

        var modal = $(this)
        modal.find('form').attr('action','/roles/' + rol_id)
    })
</script>

@stop

@section('content')

{{-- Setup data for datatables --}}
@php
$config = [
"tooltip" => "true",
];
$heads = [
'Id',
['label' => 'Rol'],
'Slug',
['label' => 'Permissions'],
['label' => 'Actions', 'no-export' => true],
];

@endphp

<div class="row py-lg-2">
    <div class="col-md-6 float-left">
        <h2><i class="fas fa-users"></i> Lista de roles</h2>
    </div>
    <div class="col-md-6"><a href="/roles/create" class="btn btn-light btn-lg float-right" role="button"
            aria-pressed="true"><i class="fas fa-user-plus"></i> Crear rol</a></div>
</div>
<hr>
<x-adminlte-datatable id="tabla-roles" :heads="$heads" head-theme="light" class="bg-teal" :config="$config ?? ''"
    striped hoverable with-buttons>
    @foreach ($roles as $rol)
    <tr>
        <td class="text-center">{{$rol['id']}}</td>
        <td>{{$rol['name']}}</td>
        <td>{{$rol['slug']}}</td>
        <td>
            @if ($rol->permissions != null)
                @foreach ( $rol->permissions as $permission )
                    <span class="badge badge-secondary">
                        {{ $permission->name }}
                    </span>
                @endforeach
            @endif
        </td>
        <td class="text-center">
            <a class="text-info p-2 fas fa-eye" href="/roles/{{ $rol['id'] }}" data-toggle="tooltip"
                data-placement="top" title="Ver"></a>
            <a class="text-success p-2 fas fa-edit" href="/roles/{{ $rol['id'] }}/edit" data-toggle="tooltip"
                data-placement="top" title="Editar"></a>
            <a class="text-danger p-2" href="#" data-toggle="modal" data-target="#deleteRol"
                data-rolid="{{ $rol['id'] }}"><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top"
                    title="Eliminar"></i></a>
        </td>
    </tr>
    @endforeach
</x-adminlte-datatable>
<x-adminlte-modal id="deleteRol" title="Eliminar rol" icon="fas fa-user-slash">
    <div class="text-center">
        <h4 class="text-danger"><i class="fas fa-biohazard"></i> Advertencia</h4>
        <p>Esta seguro de que realmente quiere eliminar a este rol del sistema.</p>
    </div>
    <x-slot name="footerSlot">
        <form action="" method="post">
            @method('DELETE')
            @csrf
            <x-adminlte-button class="mr-auto" theme="success" label="Sí eleiminar" icon="far fa-thumbs-down"
                onclick="$(this).closest('form').submit();" />
        </form>
        <x-adminlte-button theme="danger" label="Cancelar" data-dismiss="modal" icon="far fa-times-circle" />
    </x-slot>
</x-adminlte-modal>

@endsection