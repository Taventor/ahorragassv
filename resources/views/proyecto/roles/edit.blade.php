@extends('adminlte::page')

@section('title', 'Edición de rol')

@section('content_header')

<h1>Edición de rol</h1>

@stop

@section('css')
    <link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
@stop

@section('js')
    <script src="/js/bootstrap-tagsinput.js"></script>
    <script>
        $(document).ready(function() {
            $('#name').keyup(function(e) {
                var str = $('#name').val();
                str = str.replace(/\W+(?!$)/g, '-').toLowerCase();
                $('#slug').val(str);
                $('#slug').attr('placeholder', str);
            });
        });
    </script>
@stop

@section('plugins.bsCustomFileInput', true)

@section('content')


{{-- @dd($permisos); --}}


<div class="container-fluid bg-gradient-light">
    <div class="row justify-content-center">
        <div class="card bg-gradient-blue col-lg-5 m-3">
            <div class="card-header text-center">
                <h3>Datos del rol</h3>
            </div>
            <div class="card-body bg-light">
                <form action="/roles/{{ $rol -> id }}" method="post" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf()

                    <x-adminlte-input name="name" label="Name" placeholder="Nombre de rol"
                        label-class="text-lightblue" value="{{ $rol -> name }}">
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>

                    <x-adminlte-input name="slug" label="Slug" placeholder="Slug de rol" label-class="text-lightblue"
                        value="{{ $rol -> slug }}">
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('slug')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>
                    
                    {{-- <x-adminlte-input name="permissions" label="Permisos" placeholder="Agrege permisos" label-class="text-lightblue"
                        data-role="tagsinput" value=''>
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('permissions')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input> --}}

                    <div class="form-group row">
                        <label for="permissions" class="col-form-label text-md-right text-lightblue">Permisos</label>
                        <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2"><i class="fas fa-pen"></i></span>
                                </div>
                            <input id="permissions" name="permissions" type="text" class="form-control @error('permissions') is-invalid @enderror" label-class="text-lightblue"
                                value="@foreach ($rol -> permissions as $permission) {{ $permission -> name. "," }} @endforeach" data-role="tagsinput" required >
                    
                            @error('permissions')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <x-adminlte-button label="Actualizar" theme="dark" icon="fas fa-retweet" class="m-2 float-center"
                        type="submmit" />
                </form>
            </div>
            <card-footer>
                <a href="{{ url() -> previous() }}" class="btn btn-dark m-2 float-right"><i
                        class="far fa-arrow-alt-circle-left"></i> Regresar</a>
            </card-footer>
        </div>
    </div>
</div>

@endsection