@extends('adminlte::page')

@section('title', 'Maestro de combustibles')

@section('content_header')

<h1>Administración de combustibles</h1>

@stop

@section('plugins.Datatables', true)

@section('plugins.DatatablesPlugins', true)

@section('plugins.select2', true)

@section('js')

<script>
    $(function () {
    $('[data-toggle="tooltip"]').tooltip()
    });
</script>

<script>
    $('#deleteCombustible').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var user_id = button.data('combustibleid')

        var modal = $(this)
        modal.find('form').attr('action','/combustibles/' + combustible_id)
    })
</script>

@stop

@section('content')

{{-- Setup data for datatables --}}
@php
$config = [
"tooltip" => "true",
];
$heads = [
'Id',
['label' => 'Gasolina Regular'],
['label' => '$Precio'],
['label' => 'Gasolina Súper'],
['label' => '$Precio'],
['label' => 'Diesel'],
['label' => '$Precio'],
['label' => 'Actions', 'no-export' => true],
];

@endphp

<div class="row py-lg-2">
    <div class="col-md-6 float-left">
        <h2><i class="fas fa-users"></i> Precio de combustibles</h2>
    </div>
    <div class="col-md-6"><a href="/users/create" class="btn btn-light btn-lg float-right" role="button"
            aria-pressed="true"><i class="fas fa-user-plus"></i> Crear precios</a></div>
</div>
<hr>
<x-adminlte-datatable id="tabla-usuarios" :heads="$heads" head-theme="light" class="bg-teal" :config="$config ?? ''"
    striped hoverable with-buttons>
    @foreach ($combustibles as $combustible)
    <tr>
        <td class="text-center">{{$combustible['id']}}</td>
        <td>{{$combustible['gasolina_regular']}}</td>
        <td>{{$combustible['precio_gr']}}</td>
        <td>{{$combustible['gasolina_especial']}}</td>
        <td>{{$combustible['precio_gs']}}</td>
        <td>{{$combustible['diesel']}}</td>
        <td>{{$combustible['precio_diesel']}}</td>
        <td class="text-center">
            <a class="text-info p-2 fas fa-eye" href="/combustibles/{{ $combustible['id'] }}" data-toggle="tooltip"
                data-placement="top" title="Ver"></a>
            <a class="text-success p-2 fas fa-edit" href="/users/{{ $combustible['id'] }}/edit" data-toggle="tooltip"
                data-placement="top" title="Editar"></a>
            <a class="text-danger p-2" href="#" data-toggle="modal" data-target="#deleteCombustible"
                data-userid="{{ $combustible['id'] }}"><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top"
                    title="Eliminar"></i></a>
        </td>
    </tr>
    @endforeach
</x-adminlte-datatable>
<x-adminlte-modal id="deleteUser" title="Eliminar usuario" icon="fas fa-user-slash">
    <div class="text-center">
        <h4 class="text-danger"><i class="fas fa-biohazard"></i> Advertencia</h4>
        <p>Esta seguro de que realmente quiere eliminar a esta lista de precios del sistema.</p>
    </div>
    <x-slot name="footerSlot">
        <form action="" method="post">
            @method('DELETE')
            @csrf
            <x-adminlte-button class="mr-auto" theme="success" label="Sí eleiminar" icon="far fa-thumbs-down"
                onclick="$(this).closest('form').submit();" />
        </form>
        <x-adminlte-button theme="danger" label="Cancelar" data-dismiss="modal" icon="far fa-times-circle" />
    </x-slot>
</x-adminlte-modal>

@endsection