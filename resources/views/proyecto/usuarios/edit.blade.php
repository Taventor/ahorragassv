@extends('adminlte::page')

@section('title', 'Edición de usuario')

@section('content_header')

<h1>Edición de usuario</h1>

@stop

@section('plugins.bsCustomFileInput', true)

@section('content')

    <div class="container-fluid bg-gradient-light">
        <div class="row justify-content-center">
            <div class="card bg-gradient-blue col-lg-5 m-3">
                <div class="card-header text-center">
                    <h3>Datos del usuario</h3>
                </div>
                <div class="card-body bg-light">
                    <form action="/users/{{ $user -> id }}" method="post" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf()
            
                        <x-adminlte-input name="name" label="Name" placeholder="Nombre de usuario" label-class="text-lightblue"
                            value="{{ $user -> name }}">
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-user text-lightblue"></i>
                                </div>
                            </x-slot>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </x-adminlte-input>
            
                        <x-adminlte-input name="email" label="Email" type="email" placeholder="mail@example.com"
                            label-class="text-lightblue" value="{{ $user -> email }}">
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-at text-lightblue"></i>
                                </div>
                            </x-slot>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </x-adminlte-input>

                        <hr>
                        <div class="card-title text-center">
                            <h5>Cambio de contraseña</h5>
                        </div>
                        <br>
                        <hr>
            
                        <x-adminlte-input name="pass" label="Password" type="password" label-class="text-lightblue"
                            autocomplete="new-password">
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-key text-lightblue"></i>
                                </div>
                            </x-slot>
                            @error('pass')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </x-adminlte-input>
                        
                        <x-adminlte-input name="confirm-pass" label="Confirm Password" type="password" label-class="text-lightblue"
                            autocomplete="new-password">
                            <x-slot name="prependSlot">
                                <div class="input-group-text">
                                    <i class="fas fa-key text-lightblue"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input>

                        <x-adminlte-button label="Actualizar" theme="dark" icon="fas fa-retweet" class="m-2 float-center" type="submmit" />
                    </form>
                </div>
                <card-footer>
                    <a href="{{ url() -> previous() }}" class="btn btn-dark m-2 float-right"><i class="far fa-arrow-alt-circle-left"></i> Regresar</a>
                </card-footer>
            </div>
        </div>
    </div>

@endsection