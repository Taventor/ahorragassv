@extends('adminlte::page')

@section('title', 'Creación de usuario')

@section('content_header')

<h1>Crear nuevo usuario</h1>

@stop

@section('plugins.bsCustomFileInput', true)

@section('js')
<script>
    $(document).ready (function() {
        var permissions_box = $('permissions_box');
        var permissions_checkbox_list = $('permissions_checkbox_list');

        permissions_box.hide();

        $('#role').on('change', function () {
            var role = $(this).find(':selected');
            var role_id = role.data('role-id');
            var role_slug = role.data('role-slug');

            permissions_checkbox_list.empty();

            $.ajax({
                url: "/users/create",
                method: "get",
                dataType: "json",
                data: {
                    role_id: role_id,
                    role_slug: role_slug,
                }
            }).done(function (data) {
                console.log(data);

                permissions_box.show();

                $.each(data, function(index, element){
                    $(permissions_checkbox_list).append(
                        '<div class="custom-control custom-checkbox">'+
                            '<input type="checkbox" class="custom-control-input" name="permissions[]" id="'+element.slug+'" value="'+element.id+'" />'+
                            '<label class="custom-control-label" for="'+element.slug+'" >'+element.name+'</label>'+
                        '</div>'
                    )
                });
            });
        });
    });
</script>
@stop

@section('content')

<div class="container-fluid bg-gradient-light">
    <div class="row justify-content-center">
        <div class="card bg-gradient-blue col-lg-5 m-3">
            <div class="card-header text-center">
                <h3>Crear nuevo usario de sistema</h3>
            </div>
            <div class="card-body bg-light">
                <form action="/users" method="post" enctype="multipart/form-data">
                    @csrf()

                    <x-adminlte-input name="name" label="Name" placeholder="Digite el nombre del usuario"
                        label-class="text-lightblue" value="{{ old('name') }}" required >
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-user text-lightblue"></i>
                            </div>
                        </x-slot>
                    </x-adminlte-input>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <x-adminlte-input name="email" label="Email" type="email" placeholder="Digite un email ej. mail@example.com"
                        label-class="text-lightblue" id="email" required>
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-at text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>

                    <x-adminlte-input name="pass" label="Password" type="password" label-class="text-lightblue" placeholder="Digite una contraseña para el usuario"
                        autocomplete="new-password" id="pass" required minlength="8">
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-key text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('pass')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>

                    <x-adminlte-input name="pass_confirmation" label="Confirm Password" type="password" placeholder="Repita la contraseña para el usuario"
                        label-class="text-lightblue" autocomplete="new-password" required minlength="8">
                        <x-slot name="prependSlot">
                            <div class="input-group-text">
                                <i class="fas fa-key text-lightblue"></i>
                            </div>
                        </x-slot>
                        @error('pass_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </x-adminlte-input>

                    <div class="form-group">
                        <label for="role" class="text-lightblue">Selección de Rol</label><br>

                        <select class="form-control" name="role" id="role">
                            <option value="">Seleccione un Rol...</option>
                            @foreach ($roles as $rol)
                            <option data-role-id="{{ $rol->id }}" data-role-slug="{{ $rol->slug }}" value="{{ $rol->id }}">{{ $rol->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div id="permissions_box">
                        <label for="roles">Seleccionar permisos</label><br>
                        <div id="permissions_checkbox_list">

                        </div>
                    </div>

                    <x-adminlte-button label="Crear" theme="dark" icon="fas fa-user-check" class="m-2 float-center"
                        type="submmit" />
                </form>
            </div>
            <card-footer>
                <a href="{{ url() -> previous() }}" class="btn btn-dark m-2 float-right"><i
                        class="far fa-arrow-alt-circle-left"></i> Regresar</a>
            </card-footer>
        </div>
    </div>
</div>

@endsection