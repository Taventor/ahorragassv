<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Aplicación web para búsqueda de ofertas y promociones de combustibles y lubracantes en El Salvador">
        <meta name="keywords" content="HTML, CSS, JavaScript, Laravel, PHP, DPWEB-I">
        <meta name="author" content="Gustavo Enrique Tovar Ramos">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ahorra Gas SV</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
        <link rel="shortcut icon" href="./img/AhorraGasSv.ico" type="image/x-icon">
        <!-- <link rel="stylesheet" href="./css/app.css">  -->
        <link rel="stylesheet" href="./css/bootstrap.min.css">

        <script>
            $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>

    <body >
            @if (Route::has('login'))
                
            @endif

            @yield('content')

            <div>
                <header>
                    <nav class="navbar navbar-collapse navbar-expand-sm bg-dark navbar-dark">
                        <div class="container-fluid">
                            <a class="navbar-brand" href="#">
                                <img src="./img/Logo.png" alt="Avatar Logo" style="width:40px;" class="rounded-pill bg-light">
                            </a>
                            <ul class="navbar-nav" id="navbarToggleExternalContent">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Inicio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="./pages/acercade.html">Acerca de</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#Productos">Productos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#equipo">Equipo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Novedades</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contacto</a>
                                </li>

                                <li class="top-right links nav-item">
                                @auth
                                    <a href="{{ url('/home') }}">{{ __('Go Home')}}</a>
                                @else
                                    <a href="{{ route('login') }}" class="nav-link bi bi-person-circle ">{{ __('Login')}}</a>
                                </li>
                                
                                <li class="top-right links nav-item">
                                @if (Route::has('register'))
                                        <a href="{{ route('register') }}" class="nav-link bi bi-person-plus-fill "> {{ __('Register')}}</a>
                                    @endif
                                @endauth
                                </li>

                            </ul>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </nav>
        <!-- carusel -->
          <div class="container-fluid p-0">
            <div class="row">
                <div id="demo" class="carousel slide" data-bs-ride="carousel">

                    <!-- Indicators/dots -->
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
                        <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                        <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
                    </div>

                    <!-- The slideshow/carousel -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/Gasolinera.jpg" alt="bnr-01" class="d-block w-100">
                            <div class="carousel-caption bg-black opacity-75">
                                <h3>Ahorra Gas SV</h3>
                                <p>Para que vayas donde quieras</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="./img/Gasolinera2.jpg" alt="bnr-02" class="d-block w-100">
                            <div class="carousel-caption bg-black opacity-75">
                                <h3>Ahorra Gas SV</h3>
                                <p>Para que siempre encuentres el mejor precio.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="./img/Gasolinera3.jpg" alt="bnr-03" class="d-block w-100">
                            <div class="carousel-caption bg-black opacity-75">
                                <h3>Ahorra Gas SV</h3>
                                <p>Para que tu dinero realmente te rinda.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Left and right controls/icons -->
                    <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev" data-toggle="tooltip" data-placement="top" title="Anterior">
                        <span class="carousel-control-prev-icon"></span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next" data-toggle="tooltip" data-placement="top" title="Siguiente">
                        <span class="carousel-control-next-icon"></span>
                    </button>
                </div>
            </div>
        
        <!--  -->
    </header>

    <section class="container-fluid">
        <div class="row text-md-center">
            <h2>Productos y servicios</h2>
        </div>
        <div class="row" id="Productos">
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/Buscador.jpg" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Buscador</h4>
                        <p class="card-text">Busca tu gasolinera favorita.</p>
                        <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">Buscar</button>

                        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                            <div class="offcanvas-header">
                                <h5 id="offcanvasRightLabel">Buscador</h5>
                                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <div class="input-group mb-5">
                                    <span class="input-group-text" id="basic-addon1"><i class="bi bi-search"></i></span>
                                    <input type="text" class="form-control" placeholder="Busca tu gasolinera" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/Registro.jpg" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Registro</h4>
                        <p class="card-text">Regístrate con nosotros.</p>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#registro">
                            Regístrate
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/Publica.jpg" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Publica</h4>
                        <p class="card-text">Da a conocer tu establecimiento.</p>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#publica">
                            Publíca
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/Rutas.png" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Rutas</h4>
                        <p class="card-text">Define tus rutas.</p>
                        <a href="./pages/rutas.html" class="btn btn-primary">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/precios.png" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Precios</h4>
                        <p class="card-text">¿Dónde encuentro el mejor precio?.</p>
                        <a href="./pages/precios.html" class="btn btn-primary">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2">
                <div class="card">
                    <img class="card-img-top" src="./img/Ofertas.jpeg" alt="Card image">
                    <div class="card-body">
                        <h4 class="card-title">Visualizador de ofertas</h4>
                        <p class="card-text">Encuentrelas mejores ofertas.</p>
                        <a href="./pages/ofertas.html" class="btn btn-primary">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modales -->
    <!-- Registro -->
    <div class="modal fade" id="registro" tabindex="-1" aria-labelledby="registro" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registro</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Correo electrónico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text">Nunca compartiremos tus datos sin tu autorización.</div>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPassword1" class="form-label">Contraseña</label>
                            <input type="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Enviarme notificaciones</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Publica -->
    <div class="modal fade" id="publica" tabindex="-1" aria-labelledby="publica" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Publica</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="Nombre" class="form-label">Nombre de establecimiento</label>
                            <input type="text" class="form-control" id="Nombre" aria-describedby="nombrelHelp">
                            <div id="nombreHelp" class="form-text">Escribe el nombre de tu establecimiento.</div>
                        </div>
                        <div class="mb-3">
                            <label for="Direccion" class="form-label">Dirección</label>
                            <input type="text" class="form-control" id="Direccion" aria-describedby="dirlHelp">
                            <div id="dirHelp" class="form-text">Digita la dirección de tu establecimiento.</div>
                        </div>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Equipo -->
    <section class="container-fluid">
        <div class="row">
            <div class="col-xl-5"></div>
            <h2 class=" col-xl-2" id="equipo">Equipo</h2>
        </div>
    </section>
    <section class="container-fluid ">
        <div class="row">
            <div class="col-lg-4 p-2">
                <div class="card" style="height: 300px;">
                    <img class="card-img-top rounded-circle w-50 mx-auto p-2 h-75" src="./img/Gustavo.jpg" alt="Card image">
                    <div class="card-body ">
                        <h4 class="card-title text-center">Gustavo</h4>
                        <p class="card-text text-center">Desarrollador | BackEnd</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2 ">
                <div class="card" style="height: 300px;">
                    <img class="card-img-top rounded-circle w-50 mx-auto p-2 h-75" src="./img/Junior.jpg " alt="Card image">
                    <div class="card-body ">
                        <h4 class="card-title text-center">Inmer Junior</h4>
                        <p class="card-text text-center ">Desarrolardor | Base de Datos</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 p-2 ">
                <div class="card" style="height: 300px;">
                    <img class="card-img-top rounded-circle w-50 mx-auto p-2 h-75" src="./img/JoseAbraham.png" alt="Card image">
                    <div class="card-body ">
                        <h4 class="card-title text-center">José Abraham</h4>
                        <p class="card-text text-center">Desarrollador | FrontEnd</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Footer -->
    <footer>
        <section class="container-fluid">
            <div class="row">
                <div class="col-xl-1"></div>
                <div class="col-xl-4">
                    <h4>Ahorra Gas SV</h4>
                    <address>
                        <p><i class="bi bi-geo-alt-fill"></i>
                            Edificio <abbr class="initialism" title="World Trade Center"><b>WTC</b></abbr> El Salvador - 87 Avenida Norte y Calle el Mirador, Colonia Escalón
                        </p>
                            <i class="bi bi-envelope-fill"></i> info@ahorragassv.com
                        </a>
                    </address>

                </div>
                <div class="col-xl-3">
                    <h4>Empresa</h4>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a href="./pages/acercade.html" class="nav-link">Acerca de</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Equipo</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Contacto</a></li>
                    </ul>
                </div>
                <div class="col-xl-3">
                    <h4>Informacion</h4>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a href="#Productos" class="nav-link">Productos</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Novedades</a></li>
                    </ul>
                </div>
                <div class="col-xl-1"></div>
            </div>
        </section>
    </footer>
    </body>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js "></script>
        <script src="./js/app.js/"></script>    
</html>
