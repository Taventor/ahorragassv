<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Rutas para administrar usuarios */
Route::resource('users', 'UsersController')->names('users');

/* Rutas para administrar roles */
Route::resource('roles', 'RolesController')->names('roles');

/* Rutas para administrar gasolineras */
Route::resource('gasolineras', 'GasolinerasController')->names('gasolineras');

/* Rutas para administrar combustibles */
Route::resource('combustibles', 'CombustiblesController')->names('combustibles');
