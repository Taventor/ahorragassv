<div {{ $attributes->merge(['class' => "p-0 col-{$size}"]) }}>

    <span class="nav-link">

        {{-- Icon --}}
        @isset($icon)
            <i class="{{ $icon }}"></i>
        @endisset

        {{-- Header --}}
        @isset($title)
            @if(! empty($url))
                <a href="{{ $url }}">{{ __($title) }}</a>
            @else
                {{ __($title) }}
            @endif
        @endisset

        {{-- Text --}}
        @isset($text)
            <span class="{{ $makeTextWrapperClass() }}">
                {{ __($text) }}
            </span>
        @endisset

    </span>

</div>
