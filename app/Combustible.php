<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combustible extends Model
{
    public function gasolineras(){
        return $this->hasOne('App\Gasolinera');
    }
}
