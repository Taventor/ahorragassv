<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gasolinera extends Model
{
    public function combustibles()
    {
        return $this->belongsTo('App\Combustible');
    }
}
