<?php

namespace App\Http\Controllers;

use App\Combustible;
use Illuminate\Http\Request;

class CombustiblesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combustibles = Combustible::orderBy('id', 'desc')->get();
        return view('proyecto.combustibles.index', ['combustibles' => $combustibles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Combustible  $combustible
     * @return \Illuminate\Http\Response
     */
    public function show(Combustible $combustible)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Combustible  $combustible
     * @return \Illuminate\Http\Response
     */
    public function edit(Combustible $combustible)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Combustible  $combustible
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Combustible $combustible)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Combustible  $combustible
     * @return \Illuminate\Http\Response
     */
    public function destroy(Combustible $combustible)
    {
        //
    }
}
