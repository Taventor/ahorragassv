<?php

namespace App\Http\Controllers;

use App\Gasolinera;
use Illuminate\Http\Request;

class GasolinerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gasolineras = Gasolinera::orderBy('id', 'desc')->get();
        return view('proyecto.gasolineras.index', ['gasolineras' => $gasolineras]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gasolinera  $gasolinera
     * @return \Illuminate\Http\Response
     */
    public function show(Gasolinera $gasolinera)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gasolinera  $gasolinera
     * @return \Illuminate\Http\Response
     */
    public function edit(Gasolinera $gasolinera)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gasolinera  $gasolinera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gasolinera $gasolinera)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gasolinera  $gasolinera
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gasolinera $gasolinera)
    {
        //
    }
}
