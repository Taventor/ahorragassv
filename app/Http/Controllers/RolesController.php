<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('id','desc')->get();
        return view('proyecto.roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proyecto.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validación de datos de formulario
        $request->validate([
            'name' => 'required|unique:roles|max:255',
            'slug' => 'required|unique:roles|max:255',
        ]);

        $rol = new Role;
        $rol->name = $request->name;
        $rol->slug = $request->slug;
        $rol->save();

        $listOfPermissions = explode(',', $request->permissions);
        
        foreach ($listOfPermissions as $permission) {
            $permissions = new Permission();
            $permissions->name = $permission;
            $permissions->slug = strtolower(str_replace(" ", "-", $permission));
            $permissions->save();
            $rol->permissions()->attach($permissions->id);
            $rol->save();
        }

        return redirect('/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('proyecto.roles.show', ['rol' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permisos = Permission::all();
        return view('proyecto.roles.edit', ['rol' => $role], compact('permisos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        // Validación de datos de formulario
        $request->validate([
            'name' => 'required|max:255',
            'slug' => 'required|max:255',
        ]);
        if ($request->name != null && $request->name != $role->name) {
            $role->name = $request->name;
        }

        if ($request->slug != null && $request->slug != $role->slug) {
            $role->slug = $request->slug;
        }

        $role->save();

        $role->permissions()->delete(); 
        $role->permissions()->detach();

        $listOfPermissions = explode(',', $request->permissions);

        foreach ($listOfPermissions as $permission) {
            $permissions = new Permission();
            $permissions->name = $permission;
            $permissions->slug = strtolower(str_replace(" ", "-", $permission));
            $permissions->save();
            $role->permissions()->attach($permissions->id);
            $role->save();
        }

        return redirect('/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->permissions()->delete();
        $role->delete();
        $role->permissions()->detach();

        return redirect('/roles');
    }
}
