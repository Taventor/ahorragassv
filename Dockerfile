FROM php:fpm

RUN apt-get update && apt-get install -y software-properties-common \
    autoconf \
    libmcrypt-dev \
    apt-utils \
    libmagic-dev \
    libzip-dev \
    npm \
    perl \
    #php-imagick \
    #php-imagick-all-dev \
    #php7.4-imagick \
    #php-pear \
    libonig-dev \
    unzip \
    zlib1g-dev

#RUN docker-php-ext-enable imagick
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install zip
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install tokenizer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /app
